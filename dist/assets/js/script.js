$(document).ready(function(){    
    let loader = $('#loader');
    
    setTimeout(function(){
        loader.fadeOut('slow');
    }, 500);
    
        
    $(window).on('beforeunload', function() {
        loader.show();
    });
});

new WOW().init();

$(document).ready(function(){
    let top_navbar = $(document).find('.top-navbar');
    let logo = top_navbar.find('.navbar-brand img');
    
    $('.navbar-toggler').click(function(){
        logo.toggle();
        top_navbar.toggleClass('opened');
    });
});

$(document).ready(function(){
    $('.testimonials').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        items: 1,
        dots: true,
        autoplay: true,
        autoplayTimeout:5000
    });
});

$(document).ready(function(){
    $('.brands').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        items: 5,
        dots: false,
        autoplay: true,
        autoplayTimeout:5000,
        responsive: {
            0: {
                items: 3
            },
            992: {
                items: 5
            }
        }
    });
});

$('.scrollTo').click(function(e){
    let href = $(this).attr('href');
    let has_hash = href && href.includes('#');   
    
    if(has_hash){        
        let hash = href.split('#')[1];
        let element = $(document).find('#'+hash);
        
        if(element.length > 0){
            e.preventDefault();
            element.get(0).scrollIntoView({behavior: 'smooth', block: 'start', inline: "start"});
        }
    }
});


$('.top-navbar .nav-item').click(function(){
    let menu = $('.top-navbar');
    menu.find('.nav-item.active').removeClass('active');
    $(this).addClass('active');
});

$('section').each(function(){
    let id = $(this).attr('id');
    let menu = $('.top-navbar');    

    let menu_element = menu.find( "a[href$='#"+id+"']" );

    $(this).waypoint({
        handler: function() {
            menu.find('.nav-item.active').removeClass('active');

            if(menu_element.length > 0){
                menu_element.parent('li').addClass('active');
            }
        },
        offset: 50
    });   
    
});